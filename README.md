#HTCondor 
## How to submit HTCondor jobs
condor_submit CondorOption.sub 

## The useful HTCondor commands
* condor_q 
  - Check the queing status
##TauIDMCSample
Descriptions for each file :
<dl>
  <dt>Condor\_SimTf.sub</dt>
    <dd>This is a job option for Geant4 simulation.</dd>
  <dt>Condor\_RecoTf.sub</dt>
    <dd>This is a job option for reconstruction simulation.</dd>
  <dt>GenTf\_WrapperScript.sh</dt>
  <dt>mcHitRDO.py</dt>
  <dt>SimTf\_WrapperScript.sh</dt>
    <dd>This is a wrapper script for Geant4 simulation.</dd>
  <dt>RecoTf\_WrapperScript.sh</dt>
    <dd>This is a wrapper script for reconstruction simulation.</dd>
  <dt>SimTfArgument.txt</dt>
  <dt>single\_tau.py:</dt>
</dl>
