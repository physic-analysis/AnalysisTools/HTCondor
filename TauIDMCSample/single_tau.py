evgenConfig.description = "Low-pT inelastic minimum bias events for pile-up, with the A2 MSTW2008LO tune"
evgenConfig.keywords = ["QCD", "minbias", "pileup"]

from ParticleGenerator.ParticleGeneratorConf import ParticleGeneratorWithInTimePileup
topAlg += ParticleGeneratorWithInTimePileup("ParticleGeneratorWithInTimePileup")

topAlg.ParticleGeneratorWithInTimePileup.OutputLevel = INFO
topAlg.ParticleGeneratorWithInTimePileup.CollisionEnergy = 13000.0
topAlg.ParticleGeneratorWithInTimePileup.Beam1 = "PROTON"
topAlg.ParticleGeneratorWithInTimePileup.Beam2 = "PROTON"
topAlg.ParticleGeneratorWithInTimePileup.pdgID  = 15
topAlg.ParticleGeneratorWithInTimePileup.pTlow  = 500000.0
topAlg.ParticleGeneratorWithInTimePileup.pThigh = 500000.0
topAlg.ParticleGeneratorWithInTimePileup.etalow  = -2.5
topAlg.ParticleGeneratorWithInTimePileup.etahigh =  2.5
topAlg.ParticleGeneratorWithInTimePileup.positionXmin =  0.0
topAlg.ParticleGeneratorWithInTimePileup.positionXmax =  0.0
topAlg.ParticleGeneratorWithInTimePileup.positionYmin =  0.0
topAlg.ParticleGeneratorWithInTimePileup.positionYmax =  0.0
topAlg.ParticleGeneratorWithInTimePileup.positionZmin =  0.0
topAlg.ParticleGeneratorWithInTimePileup.positionZmax =  0.0
topAlg.ParticleGeneratorWithInTimePileup.deltaR  =  0.8
topAlg.ParticleGeneratorWithInTimePileup.NumberOfInTimePileup = 0
topAlg.ParticleGeneratorWithInTimePileup.BeamSpotSigmaX = 0.015
topAlg.ParticleGeneratorWithInTimePileup.BeamSpotSigmaZ = 75.0

evgenConfig.generators += ["ParticleGenerator"]

topAlg.ParticleGeneratorWithInTimePileup.Commands += [
 "Main:timesAllowErrors = 500",
 "6:m0 = 172.5",
 "23:m0 = 91.1876",
 "23:mWidth = 2.4952",
 "24:m0 = 80.399",
 "24:mWidth = 2.085",
 "StandardModel:sin2thetaW = 0.23113",
 "StandardModel:sin2thetaWbar = 0.23146",
 "ParticleDecays:limitTau0 = on",
 "ParticleDecays:tau0Max = 10.0"]

topAlg.ParticleGeneratorWithInTimePileup.Commands += [
 "Tune:pp = 5",
 "PDF:useLHAPDF = on",
 "PDF:LHAPDFset = MSTW2008lo68cl.LHgrid",
 "MultipartonInteractions:bProfile = 4",
 "MultipartonInteractions:a1 = 0.03",
 "MultipartonInteractions:pT0Ref = 1.90",
 "MultipartonInteractions:ecmPow = 0.30",
 "BeamRemnants:reconnectRange = 2.28",
 "SpaceShower:rapidityOrder=0"]
evgenConfig.tune = "A2 MSTW2008LO"

topAlg.ParticleGeneratorWithInTimePileup.Commands += \
    ["SoftQCD:minBias = on",
     "SoftQCD:singleDiffractive = on",
     "SoftQCD:doubleDiffractive = on"]

