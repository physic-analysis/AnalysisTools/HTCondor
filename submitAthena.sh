#!/bin/sh

inFile=$1
outputFile=${inFile##*EXT1._}

cd /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/AnalysisArea/build
##  You will need this bit for every Athena job
# non-interactive shell doesn't do aliases by default. Set it so it does
#shopt -s expand_aliases
## set up the aliases.
## note that ATLAS_LOCAL_ROOT_BASE (unlike the aliases) is passed the shell from where you are submitting.
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh'
#
## now proceed normally to set up the other aliases
#setupATLAS
#asetup 21.2.48,AnalysisBase
#source /afs/cern.ch/user/k/ktakeda/workspace/public/atlas-phys/AJ/WG1/SingleParticle/AnalysisArea/build/x86_64-slc6-gcc62-opt/setup.sh

#athena.py TauAnalysis/JobOptionForCondor.py --filesInput=${inFile} -c "outputFile='HighPtTau.1000GeV.CondorOutput_${outputFile}';"
#athena.py TauAnalysis/JobOptionForCondor.py --filesInput=${inFile} -c "outputFile='LightQuarkJet.1000GeV.CondorOutput_${outputFile}';"
#athena.py TauAnalysis/JobOptionForCondor.py --filesInput=${inFile} -c "outputFile='LightQuarkJet.100GeV.CondorOutput_${outputFile}';"
athena.py TauAnalysis/JobOptionForCondor.py --filesInput=${inFile} -c "outputFile='HighPtTau.1TeV_v2_${outputFile}';"
