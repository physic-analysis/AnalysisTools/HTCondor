#!/bin/sh

# If HTCondor service failed to do your jobs, you collect the failed output data using this script.
# 


DATA_PATH=/afs/cern.ch/user/k/ktakeda/workspace/public/Data/AJ/SingleTau/xAOD/user.ktakeda.HighPtTauPerformance.TaupT1000GeV.100000.events.MC12G4.GitTag-01-00-05_EXT1/user.ktakeda.16081327.EXT1._
#DATA_PATH=/afs/cern.ch/user/k/ktakeda/workspace/public/Data/AJ/SingleTau/xAOD/user.ktakeda.HighPtTauPerformance.LightQuarkJetPt1000GeV_ATLAS_Standard.100000events.FullG4.RECO-04-00-01_EXT1/user.ktakeda.16480074.EXT1._
rm RetryDataPath.txt
ls -lht | grep $1 | grep -v K | grep -v merged | awk '{print $9;}' |  while read line
do 
  echo $line
  retry_data=${line##*HighPtTau.1TeV_v2_}
  echo $DATA_PATH$retry_data >> RetryDataPath.txt
done
